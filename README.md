# Curso Análisis Estadístico

Apuntes al Curso de análisis estadistico de la universidad EAN M4

## Sesión 1

En la sesión 1 trabajammos familiarización con los conceptos de estadistica. Los ejercicios pendientes por trabajar son:

* Conformación de Grupos
* Familiarización de las herramientas SPSS y R (instalar la versión del profe)
* Estudiar los conceptos de:
  * Media
  * Mediana
  * Desviaciones
  * Outlayers
* Repasar el video y tratar de replicar los ejercicios

## Sesión 2 

