# Instalamos el paquete para leer excel
# install.packages("readxl")
# install.packages("ggplot2")

# Librería para excel
library("readxl")

# Librería para gráficos
library("ggplot2")

# Leemos los datos
mis_datos = read_excel("./Sesion1/BaseEjemploEstudiantes.xlsx")

# Imprimimos la data
print(names(mis_datos))

# Calculamos el resumen por quartiles para la edad
resumen <- summary(mis_datos$Edad)

# Imprimimos el resumen
print(resumen)

# Graficamos la data
plot(mis_datos$Edad)

# Histograma simple
hist(mis_datos$Edad)

# box plot de ejemplo
boxplot(Edad~Tiempo,data=mis_datos, main="Datos de estudiantes",
   xlab="Tiempo de Viaje", ylab="Edad")